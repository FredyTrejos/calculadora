function agregarNumero(numero){
    let operacion = document.getElementById("operacion");
    operacion.value += numero;
    resultado.textContent=""
}

function operador(operador) {
    // Agrega el operador a la pantalla
    operacion.value += "" + operador + "";
    resultado.textContent=""

}

function Limpiar(){
    operacion.value = ""
    resultado.textContent= ""
}

function borrar() {
    // Elimina el último carácter de la pantalla
    operacion.value = operacion.value.slice(0, -1); // Elimina el último carácter
    resultado.textContent= ""
}

function calcular() {
    resultado = document.getElementById("resultado");
    try {
        // Evalúa la expresión aritmética en la pantalla
        let resulta = eval(operacion.value);
        resultado.textContent = resulta;
    } catch (error) {
        resultado.textContent = "Syntax Error"; // Maneja errores de evaluación
    }
}